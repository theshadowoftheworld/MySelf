﻿

namespace ImplementSort
{
    using FresherLib.Core;
    public class Sort : ISortStrategy
    {
        int[] ISortStrategy.Execute(int[] elements)
        {
            return InterchangeSort(elements);
        }

        public static int[] InterchangeSort(int []elements)
        {
            for (int i = elements.Length - 1; i > 0; i--)
            {
                for (int j = i - 1; j >= 0; j--)
                {
                    if ( elements[i] < elements[j])
                    {
                        elements[i] += elements[j];
                        elements[j] = elements[i] - elements[j];
                        elements[i] -= elements[j];
                    }
                }
            }

            return elements;
        }
    }
}
